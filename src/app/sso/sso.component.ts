import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {environment} from "../../environments/environment";

@Component({
  selector: 'app-sso',
  templateUrl: './sso.component.html',
  styleUrls: ['./sso.component.css']
})
export class SsoComponent implements OnInit {
  private returnUrl = '';
  constructor(
    private route: ActivatedRoute
  ) { }
  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    localStorage.setItem('returnUrl', this.returnUrl);
    window.location.href = 'https://localhost:9443/oauth2/authorize?client_id=' +
      environment.SSO_CLIENT_ID + "&scope=openid&redirect_uri=" +
      encodeURIComponent(environment.SSO_CALLBACK_URL) + "&response_type=code";
  }
}
