import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SsoComponent} from "./sso/sso.component";
import {FeaturesComponent} from "./features/features.component";
import {AuthGuard} from "./auth.guard";
import {AppComponent} from "./app.component";
import {SsoProcessingComponent} from "./sso-processing/sso-processing.component";

const routes: Routes = [
  {
    path: '', component: AppComponent,
    children: [
      { path: '', redirectTo: '/features', pathMatch: 'full'},
      {path: 'features', canActivate: [AuthGuard], component: FeaturesComponent},
      {path: 'auth', component: SsoComponent},
      {path: 'sso-processing', component: SsoProcessingComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
