import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-sso-processing',
  templateUrl: './sso-processing.component.html',
  styleUrls: ['./sso-processing.component.css']
})
export class SsoProcessingComponent implements OnInit {
  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) { }
  ngOnInit(): void {
    localStorage.setItem('isAuthentic', 'true'); // giả sử người dùng đã đăng nhập
    let code = this.activatedRoute.snapshot.queryParams["code"]; // lấy giá trị mà sso server trả về
    localStorage.setItem('code', code);
    this.router.navigate(['features']);
  }
}
