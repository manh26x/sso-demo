import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SsoProcessingComponent } from './sso-processing.component';

describe('SsoProcessingComponent', () => {
  let component: SsoProcessingComponent;
  let fixture: ComponentFixture<SsoProcessingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SsoProcessingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SsoProcessingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
